DESTDIR += bin
INCLUDEPATH += inc
OBJECTS_DIR = obj
MOC_DIR = src
QT += opengl
QT += sql
LIBS += -lglut -lGLU

HEADERS +=                              \
  inc/MainWindow.hpp                    \
  inc/ProfileTab.hpp                    \
  inc/MaterialTab.hpp                   \
  inc/DecoTab.hpp                       \
  inc/ColorTab.hpp                      \
  inc/AView.hpp                         \
  inc/SideView.hpp                      \
  inc/GlobalView.hpp                    \
  inc/UpView.hpp                        \
  inc/Montgolfier.hpp                   \
  inc/Surface.hh                        \
  inc/CastelJau.hh                      \
  inc/Flattening.hh                     \
  inc/SpindleGenerator.hh               \
  inc/BallonSQL.hpp                     \
  inc/ProductTab.hpp

SOURCES +=                              \
  src/main.cpp                          \
  src/MainWindow.cpp                    \
  src/ProfileTab.cpp                    \
  src/MaterialTab.cpp                   \
  src/DecoTab.cpp                       \
  src/ColorTab.cpp                      \
  src/graphicEngine/SideView.cpp        \
  src/graphicEngine/GlobalView.cpp      \
  src/graphicEngine/UpView.cpp          \
  src/graphicEngine/Montgolfier.cpp     \
  src/graphicEngine/Surface.cpp         \
  src/graphicEngine/CastelJau.cpp       \
  src/graphicEngine/Flattening.cpp      \
  src/graphicEngine/SpindleGenerator.cpp\
  src/database/connect.cpp              \
  src/database/BallonSQL.cpp            \
  src/database/select.cpp               \
  src/database/update.cpp               \
  src/database/delete.cpp               \
  src/database/insert.cpp               \
  src/ProductTab.cpp
