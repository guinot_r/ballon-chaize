#include	<iostream>
#include	"SideView.hpp"

SideView::SideView(Montgolfier * balloon) : AView()
{
  _montgol = balloon;
}

void		SideView::initializeGL()
{
  glShadeModel(GL_SMOOTH);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void		SideView::resizeGL(int w, int h)
{
  if (h == 0)
    h = 1;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0f, (GLfloat)w/(GLfloat)h, 0.1f, 100.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

/*
** Dessine la montgolfière
*/
void		SideView::paintGL()
{
  _montgol->draw(4.0f, 2.0f, 5.0f, 4.0f); //Xoeil, Yoeil, Zoeil, Ylookat
}

/*
** Gère les évènements sur la fenêtre
*/
void		SideView::mousePressEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton)
    {
      std::cout << "Vue side" << std::endl;
    }
  else
    {
      QGLWidget::mousePressEvent(event);
    }
}

