#include	"Montgolfier.hpp"

GLfloat		_ctrlpoints[5][3] = {	// Les points de controles de la montgolfiere sont set ici 
  {0.0, 5.0, 0.0},
  {1.0, 7.0, 0.0},
  {2.0, 2.0, 0.0},
  { 4.0, 5.0, 0.0},
  {2.0, 2.0, 0.0}  
};

GLfloat		_galbe[5][3] = {	// Les points de controles du galbe  sont set ici 
  {0.0, 7.0, 0.0},
  {1.0, 6.5, 0.0},
  {5, 2, 0.0},
  {5, 6.5, 0.0},
  {2.0, 2.0, 0.0} 
};

Montgolfier::Montgolfier()
{
  _degreeRotationX = 0.f;
  _degreeRotationY = 0.f;
  _zoom = 0.0f;
  _nbFuseau = 16;			// Le nombre de fuseau
  _surface = new Surface[_nbFuseau];	// Surface correspond à un fuseau
  _controlPoints.resize(5);		// On peut mettre autant de points de controles que l'on veut en adaptant le double tableau ci dessus
  _curve.resize(5);			// Tout doit être resize à la même taille
  _shapeLiness.resize(5);		// De même ici
  _precision = 20;			// précision du quadrillage
}

Montgolfier::~Montgolfier()
{

}

void		Montgolfier::init()
{
  int		i;
   
  _isRunning = true;
  for (i = 0; i < 5; ++i)
    {
      _controlPoints[i] = glm::vec4(_ctrlpoints[i][0], _ctrlpoints[i][1], _ctrlpoints[i][2], 1); //initialisation des points de contrôle
      _curve[i] = glm::vec2(_ctrlpoints[i][0],_ctrlpoints[i][1]); // initalisation des courbes
    }
  for (i = 0; i < 5; ++i)
    {
      _shapeLiness[i] = glm::vec2(_galbe[i][0], _galbe[i][1]);	// initialisation du galbe
    }
  _spindleGenerator.setCurve(_curve);				// A partir d'une courbe 2D on va générer les points des surfaces de bézier (cette classe génére les points de toutes les surfaces) 
  _spindleGenerator.setShapeLiness(_shapeLiness);		// De même pour le galbe
  _spindles = _spindleGenerator.getSpindles(_nbFuseau);		// on recupère les fuseaux
  for (i = 0; i < _nbFuseau; ++i)
    {
      _surface[i].setMatrix(_spindles[i]);			// on lui set les points de controles 
      _surface[i].build(_precision);				// on génère tous les autres points à partir des points de contrôles
    }
}

void		Montgolfier::draw(float x, float y, float z, float lookY)
{
  int		i;

  glMatrixMode(GL_PROJECTION);

  glLoadIdentity();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  gluPerspective(60, 1 ,1, -1);
  gluLookAt(x, y, z, 0,lookY,0, 0,1,0);
  //gluLookAt(3 + _zoom, 6 + _zoom, 4 + _zoom, 0,0,0, 0,1,0); 
  glColor3f(1.0, 1.0, 1.0);
  glPushMatrix ();
  glEnable(GL_DEPTH_TEST);
  glRotatef(_degreeRotationX, 1.0, 0.0, 0.0);  
  glRotatef(_degreeRotationY, 0.0, 1.0, 0.0);  
  for (i = 0; i < _nbFuseau; ++i)
    {
      glColor3f(0.0f, 0.3f, 0.35f + (i % 2 * 0.2));
      _surface[i].draw();					// on parcours et dessine tous les points
    }
  glPointSize(5.0);
  glColor3f(1.0, 1.0, 0.0);
 
  glColor3f(0.0, 1.0, 0.0);					// Génération du repère
  glBegin(GL_LINE_STRIP);
  glVertex3f(0, 0, 0);
  glVertex3f(500, 0, 0);
  glEnd();

  glColor3f(0.0, 0.0, 1.0);
  glBegin(GL_LINE_STRIP);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 500, 0);
  glEnd();
  glColor3f(1.0, 0.0, 0.0);
  glBegin(GL_LINE_STRIP);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 500);
  glEnd();
  glPopMatrix ();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


void		Montgolfier::setCtrlPoints(std::list<glm::vec2> const&ctrlPoints)
{
  _ctrlPoints = ctrlPoints;
}
