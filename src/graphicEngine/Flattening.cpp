#include	"Flattening.hh"

// void				Flattening::rotateQuadrilateral(glm::vec4 **quad)
// {
// }

// void				Flattening::translateQuadrilateral(glm::vec4 **quad)
// {
// }


glm::vec4	**Flattening::generate(glm::vec4 **points, unsigned int xSize, unsigned int ySize)
{
  glm::vec4	**flattened;
  unsigned int	i, j;

  flattened = new glm::vec4*[xSize];
  for (i = 0; i < (xSize - 1 + 1); ++i)
    {
      flattened[i] = new glm::vec4[ySize];
      for (j = 0; j < (ySize - 1 + 1); ++j)
	{
	  flattened[i][j] = points[i][j];
	}
    }
  return (flattened);
}
