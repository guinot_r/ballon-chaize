#include		<iostream>
#include		<GL/gl.h>
#include		"CastelJau.hh"

CastelJau::CastelJau(std::vector<glm::vec4> const &controlPoints, double precision)
  :_controlPoints(controlPoints), _precision(precision)
{
}

CastelJau::~CastelJau()
{
}

void				CastelJau::setControlPoints(std::vector<glm::vec4> const &controlPoints)
{
  _controlPoints = controlPoints;
}

std::vector<glm::vec4> const	&CastelJau::getControlPoints() const
{
  return (_controlPoints);
}

void				CastelJau::setPrecision(double precision)
{
  _precision = precision;
}

double				CastelJau::getPrecision() const
{
  return (_precision);
}

/*
** Permet de dessiner une simple courbe de bézier
*/
void				CastelJau::draw()
{
  glm::vec4			tmp;
  GLfloat			x[3];

  glBegin(GL_LINE_STRIP);
  for (double t = 0; t <= 1; t += _precision)
    { 
      tmp = getCasteljauPoint(_controlPoints.size() - 1, 0, t); 
      x[0] = tmp.x;
      x[1] = tmp.y;
      x[2] = tmp.z;
      glVertex3fv(&x[0]);
    }
  glVertex3fv(&x[0]);
  glEnd();
}


/*
** Interpolation de vecteur par rapport a t
*/
glm::vec4			CastelJau::interpolation(glm::vec4 a, glm::vec4 b, double t)
{
  return (glm::vec4( (1 - t) * a.x + t * b.x,
		     (1 - t) * a.y + t * b.y,
		     (1 - t) * a.z + t * b.z,
		     1
		     ));
}

/*
** Fonction récursive : Permet d'avoir un point selon la valeur de t (time)
*/
glm::vec4			CastelJau::getCasteljauPoint(int r, int i, double t)
{ 
  glm::vec4			p1;
  glm::vec4			p2;

  if (r == 0)
    return (_controlPoints[i]);  
  p1 = getCasteljauPoint(r - 1, i, t);
  p2 = getCasteljauPoint(r - 1, i + 1, t);
  return (interpolation(p1, p2, t));
}
