#include		<GL/gl.h>
#include		"Surface.hh"
#include		"CastelJau.hh"

Surface::Surface()
  :_points(NULL), _precision(0)
{
}

Surface::Surface(std::vector<std::vector<glm::vec4> > const &matrix)
  :_matrix(matrix), _points(NULL), _precision(0)
{
}

Surface::~Surface()
{
  desctructPoints();
}

void			Surface::setMatrix(std::vector<std::vector<glm::vec4> > const &matrix)
{
  _matrix = matrix;
}

std::vector<std::vector<glm::vec4> > const	&Surface::getMatrix() const
{
  return (_matrix);
}

void			Surface::desctructPoints()
{
  unsigned int		i;

  if (_points != NULL)
    {
      for (i = 0; i <= _precision; ++i)
	delete [] _points[i];
      delete [] _points;
    }	  
}

glm::vec4		**Surface::getPoints() const
{
  return (_points);
}

void			Surface::setPoints(glm::vec4 **points)
{
  _points = points;
}

/*
** Genere les points du quadrillage
*/
void			Surface::build(unsigned int precision)
{
  unsigned int		u, v;

  desctructPoints();
  _precision = precision;
  if (_precision)
    {
      _points = new glm::vec4*[precision + 1];
      for (u = 0; u <= precision; ++u)
	{
	  _points[u] = new glm::vec4[precision + 1];
	  for (v = 0; v <= precision; ++v)
	    _points[u][v] = getPatchPoint((float)u / precision, (float)v / precision);
	}
    }
}

/*
** Relie les points du quadrillage et dessine un fuseau
*/
void			Surface::draw()
{
  unsigned int		i, j, color;

  color = 0;
  if (_points)
    {
      for (i = 0; i < _precision; ++i)
      	{
      	  glBegin(GL_LINE_STRIP);
      	  for (j = 0; j < _precision; ++j)
	    {
	      color = !color;
	      //glColor3f(1.0f, color, color);
	      glVertex3fv(&(_points[i][j][0]));
	      glVertex3fv(&(_points[i][j + 1][0]));
	      glVertex3fv(&(_points[i + 1][j + 1][0]));
	      glVertex3fv(&(_points[i + 1][j][0]));
	    }
	  color = !color;
      	  glEnd();
      	}
    }
}

/*
** Permet de récupérer la valeur de la courbe de bézier pour x = u et y = v
*/
glm::vec4		Surface::getPatchPoint(double u, double v) // x / y
{
  unsigned int			i;
  glm::vec4			res;
  CastelJau			casteJau;
  std::vector<glm::vec4>	controlPointsA;
  std::vector<glm::vec4>	controlPointsB;

  controlPointsA.resize(_matrix.size());
  controlPointsB.resize(_matrix.size());
  for (i = 0; i < _matrix.size(); ++i)
    {
      controlPointsA = _matrix[i];
      casteJau.setControlPoints(controlPointsA);
      controlPointsB[i] = casteJau.getCasteljauPoint(controlPointsA.size() - 1, 0, u);
    }
  casteJau.setControlPoints(controlPointsB);
  return (casteJau.getCasteljauPoint(controlPointsB.size() - 1, 0, v));
}
