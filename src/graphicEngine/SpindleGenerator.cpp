#include				<glm/gtc/matrix_transform.hpp>
#include				<glm/gtx/transform.hpp>
#include				"SpindleGenerator.hh"

SpindleGenerator::SpindleGenerator()
{
}

SpindleGenerator::SpindleGenerator(std::vector<glm::vec2> const &curve)
  :_curve(curve)
{
}

SpindleGenerator::~SpindleGenerator()
{
}

void					SpindleGenerator::setCurve(std::vector<glm::vec2> const &curve)
{
  _curve = curve;
}

std::vector<glm::vec2> const		&SpindleGenerator::getCurve() const
{
  return (_curve);
}

void					SpindleGenerator::setShapeLiness(std::vector<glm::vec2> const &shapeLiness)
{
  _shapeLiness = shapeLiness;
}

std::vector<glm::vec2> const		&SpindleGenerator::getShapeLiness() const
{
  return (_shapeLiness);
}

/*
** Permet de générer les points de contrôle d'une surface de bézier
*/
std::vector<std::vector<std::vector<glm::vec4> > >	SpindleGenerator::getSpindles(unsigned int spindleNumber) const
{
  std::vector<std::vector<std::vector<glm::vec4> > >	spindles;
  float							degree = 0;
  unsigned int						i;
  unsigned int						j;
  unsigned int						k;
  unsigned int						galbeDegree = 3;
  glm::mat4						rotateMatrix;
  std::vector<glm::vec2> const				*currentCurve;

  spindles.resize(spindleNumber);
  for (i = 0; i < spindleNumber; ++i)
    {
      spindles[i].resize(galbeDegree);
      for (j = 0; j < galbeDegree; ++j)
	{
	  if (j == 1 && _shapeLiness.size())
	    currentCurve = &_shapeLiness;
	  else
	    currentCurve = &_curve;
	  spindles[i][j].resize(currentCurve->size());
	  rotateMatrix = glm::rotate(glm::mat4(1.f),
				     degree + (j * ((360.0f / spindleNumber) / (float(galbeDegree - 1)))),
				     glm::vec3(0.f, 1.f, 0.f));
	  for (k = 0; k < currentCurve->size(); ++k)
	    spindles[i][j][k] = rotateMatrix * glm::vec4((*currentCurve)[k], 0.0f, 1.0f);
	}
      degree += 360.0f / ((float)spindleNumber);
    }
  return (spindles);
}
