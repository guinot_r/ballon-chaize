//
// ProfileTab.cpp for Ballon Chaize in /home/chaume_c/Projects/Other/QT_test
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Sat Dec 14 01:38:59 2013 christian chaumery
// Last update Thu Jan 16 01:57:20 2014 christian chaumery
//


#include	"ProfileTab.hpp"

ProfileTab::ProfileTab() : QWidget()
{
  _mainLayout = new QGridLayout;
  _montgol = new Montgolfier;

  _views[ProfileTab::SIDEVIEW] = new SideView(_montgol); // On set toutes les vues et on leurs donne un accès à la montgolfière
  _views[ProfileTab::UPVIEW] = new UpView(_montgol);
  _views[ProfileTab::GLOBALVIEW] = new GlobalView(_montgol);
  _mainLayout->addWidget(_views[ProfileTab::GLOBALVIEW], 0, 2, 2, 1); // On ajoute des contextes opengl et leurs position 
  _mainLayout->addWidget(_views[ProfileTab::SIDEVIEW], 1, 1);
  _mainLayout->addWidget(_views[ProfileTab::UPVIEW], 0, 1);
  setLayout(_mainLayout);
}

ProfileTab::~ProfileTab()
{
  
}

/*
  On parcours les vues et on les initialise
 */

void ProfileTab::init(/*const std::list<glm::vec2> &ctrlPoints*/)
{
  for (std::map<Views, AView *>::iterator it = _views.begin() ; it != _views.end(); ++it)
    {
      it->second->initializeGL();
    }
}

/*
  On parcours les vues et on les dessine
 */

void ProfileTab::draw()
{
  for (std::map<Views, AView *>::iterator it = _views.begin() ; it != _views.end(); ++it)
    it->second->paintGL();
}
