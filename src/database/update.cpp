//
// update.cpp for sql in /home/TaeguKazu/Documents/BallonChaize
// 
// Made by Kevin Dafonseca
// Login   <TaeguKazu@epitech.net>
// 
// Started on  Thu Mar  6 09:16:59 2014 Kevin Dafonseca
// Last update Sun Mar  9 05:00:09 2014 Christian CHAUMERY
//

/*
** Ce fichier contient la/les fonction(s) suivante(s): bool		BallonSQL::execUpdate(std::string table_name, std::string col_name, std::string value, std::string where)
**
** La fonction execUpdate crée la requète de type:
** UPDATE table_name   SET col_name=value   WHERE where_definition
** (Ce référencer au MCD pour plus de précision sur la construction des tables.)
**
** Celle-ci sera ensuite exécutée via la méthode exec() de l'objet QSqlQuery nommé "q", cette méthode prend en paramètre la requète précédemment creer.
** La fonction retournera TRUE en cas de réussite de l'exécution de la fonction au sein de la base de données, sinon FALSE en cas d'erreur.
**
** Exemple d'appel de la fonction :
**
** void	SqlUpdate(BallonSQL *db)
** {
** 		if (db->execUpdate("Accastillage", "nom_a", "Testouille", "id_a=1") == false)
			std::cerr << "Error on execUpdate" << std::endl;
** }
*/

#include "BallonSQL.hpp"

bool						BallonSQL::execUpdate(std::string table_name, std::string col_name, std::string value, std::string where)
{
  QString	final_q;
  QSqlQuery	q;

      std::string query = "UPDATE ";
      
      query += table_name;
      query += " SET " + col_name + "='" + value + "' WHERE " + where;
      if (q.exec(final_q.fromStdString(query)))
	{
	  while (q.next())
	    {
	    }
	}
      else
	{
	  std::cout << "Une erreur s'est produite." << std::endl;
	  std::cerr << q.lastError().text().toStdString() << std::endl;  
	  return (false);
	}
      return (true);
}

