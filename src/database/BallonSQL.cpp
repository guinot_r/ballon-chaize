/*
** Ce fichier contient la/les fonction(s) suivante(s):  BallonSQL::BallonSQL(std::string host, std::string user, std::string password, std::string name)
                                                        BallonSQL::BallonSQL(BallonSQL const& ptr)
                                                        BallonSQL::~BallonSQL()
                                                        BallonSQL& BallonSQL::operator=(BallonSQL const& ptr)
                                                        QSqlDatabase  BallonSQL::getDb() const
                                                        bool          BallonSQL::open()
                                                        std::string   BallonSQL::lastError()
                                                        std::string   BallonSQL::hostName()
**
** La Classe BallonSQL est une classe ayant la forme de Coplien.
** Le constructeur par défaut prendre 4 paramètres:
**    le premier est l'IP du serveur de la base de donnée ou localhost
**    le second est le nom de l'utilisateur sur la base de donnée MYSQL
**    le troisieme est le mot de passe de l'utilisateur
**    le quatrieme est le nom de la base de donnée
**
** La fonction open() renvoie TRUE ci la connection à la base de donnée est faite, sinon elle retourne FALSE.
**
** La fonction lastError() retourne le message de la derniere instruction SQL executer ayant échoué.
*/

#include "BallonSQL.hpp"

BallonSQL::BallonSQL(std::string host, std::string user, std::string password, std::string name)
{
  QString str;
  
  _db = QSqlDatabase::addDatabase(str.fromStdString("QMYSQL"));
  _db.setHostName(str.fromStdString(host));
  _db.setUserName(str.fromStdString(user));
  _db.setPassword(str.fromStdString(password));
  _db.setDatabaseName(str.fromStdString(name));
}

BallonSQL::BallonSQL(BallonSQL const& ptr)
{
  _db = ptr._db;
}

BallonSQL::~BallonSQL()
{
  _db.close();
}

BallonSQL& BallonSQL::operator=(BallonSQL const& ptr)
{
  _db = ptr.getDb();
  _query = ptr._query;
  return (*this);
}

QSqlDatabase	BallonSQL::getDb() const
{
  return (_db);
}

bool		BallonSQL::open()
{
  return (_db.open());
}

std::string	BallonSQL::lastError()
{
  return (_db.lastError().text().toStdString());
}

std::string	BallonSQL::hostName()
{
  return (_db.hostName().toStdString());
}
