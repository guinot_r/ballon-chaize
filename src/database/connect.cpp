#include <QCoreApplication>
#include <QtSql>
#include <string>
#include <iostream>
#include "BallonSQL.hpp"

#define q2c(string)

void	mallselect(BallonSQL *db)
{
  std::vector< std::vector< std::string > >	tab;

  tab = db->selectAll("TOTO");
  std::cout << "Debut de laffichage du tableau retour, Select All" << std::endl;
  for (unsigned int i=0; i < tab.size(); ++i)
    {
      std::cout << "=====================================    Ligne " << i + 1 << "   =====================================" << std::endl;
      for (unsigned int j=0; j < tab[i].size(); ++j)
	{
	  std::cout <<  tab[i][j] << std::endl;
	}
    } 
  std::cout << "Fin de l'affichage du tableau" << std::endl;
}

void	mproviderselect(BallonSQL *db)
{
  std::vector< std::vector< std::string > >	tab;

  tab = db->selectProvider("Accastillage");
  std::cout << "Fin du Select / Debut de laffichage du tableau retour" << std::endl;
  for (unsigned int i=0; i < tab.size(); ++i)
    {
      std::cout << "=====================================    Ligne " << i + 1 << "   =====================================" << std::endl;
      for (unsigned int j=0; j < tab[i].size(); ++j)
	{
	  std::cout <<  tab[i][j] << std::endl;
	}
    } 
  std::cout << "Fin de l'affichage du tableau" << std::endl;
}

void	mupdate(BallonSQL *db)
{
  std::cout << "before update" << std::endl;
  db->execUpdate("Accastillage", "nom_a", "Testouille", "id_a=1");
  std::cout << "update done" << std::endl;
}

void	minsert(BallonSQL *db)
{
  std::list <std::string> vlist;

  vlist.push_back("value1");
  vlist.push_back("value2");
  vlist.push_back("value3");
  vlist.push_back("value4");
  vlist.push_back("value5");
  std::cout << "before insert" << std::endl;
  db->execInsert("Cordage", vlist);
  std::cout << "insert done" << std::endl;
  
}

void	mdelete(BallonSQL *db)
{
  std::cout << "before delete" << std::endl;
  db->DeleteById("Cordage", 2);
  std::cout << "delete done" << std::endl;  
}


void	connect()
{
  BallonSQL	db("localhost", "root", "alacon", "Ballon_Chaize");

  if (db.open())
      std::cout << "Vous êtes maintenant connecté à " << db.hostName() << std::endl;
  else
    {
      std::cout << "La connexion a échouée." << std::endl;
      std::cerr << db.lastError() << std::endl;
    }

  mallselect(&db);
  mproviderselect(&db);
  mupdate(&db);
  minsert(&db);
  mdelete(&db);
}
