//
// BallonSQLselect.cpp for sqk in /home/TaeguKazu/Documents/BallonChaize
// 
// Made by Kevin Dafonseca
// Login   <TaeguKazu@epitech.net>
// 
// Started on  Thu Mar  6 09:17:08 2014 Kevin Dafonseca
// Last update Sun Mar  9 05:00:16 2014 Christian CHAUMERY
//

/*

** Ce fichier contient la/les fonction(s) suivante(s):  std::vector< std::vector< std::string > > BallonSQL::execSelect(std::string name, std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectAll(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectName(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectWeight(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectResist(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectResistX(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectResistY(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectPrice(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectSchema(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectProvider(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectColor(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectSizeLaies(std::string table)
                                                        std::vector< std::vector< std::string > > BallonSQL::selectProviderAdress()
                                                        std::vector< std::vector< std::string > > BallonSQL::selectProviderPhone()
**
** La fonction execUpdate est la fonction générant la requète SELECT celle-ci est appeler dans toutes les autres fonctions présentent dans ce fichier,
** elle peut aussi être appelé directement en lui donnant le "name" du champ rechercher et la "table" ou chercher.
**
** La requète executé est du type :
** SELECT name FROM table;
**
** Le retour de cette fonction est stoqué dans un vector de vector de string, chaque ligne représente une ligne retourné par le serveur MySQL lors de l'exécution de la requète,
** puis chaque colonne représente une valeur correspondant aux valeurs des colonnes présentent dans la table solicité.
** Chaque fonctions retourent un double tableau, celui-ci a une taille égal à 0 lorsqu'aucune correspondance n'a été trouvé lors de l'exécution de la requète (détectable avec la méthode size() de la classe std::vector).
** 
** Ici "X" représente la lettre associer au nom de chaque table (ce référencer au MCD pour plus de précision sur la construction des tables).
**
** La fonction selectAll(std::string table), retourne les valeurs de tous les champs de la table "table".
** La fonction selectName(std::string table), retourne la/les valeur(s) du champs "nom_X" de la table "table".
** La fonction selectWeight(std::string table), retourne la/les valeur(s) du champs "poids_X" de la table "table".
** La fonction selectResist(std::string table), retourne la/les valeur(s) du champs "resistance_X" de la table "table".
** La fonction selectResistX(std::string table), retourne la/les valeur(s) du champs "resistance_x_X" de la table "table".
** La fonction selectResistY(std::string table), retourne la/les valeur(s) du champs "resistance_y_X" de la table "table".
** La fonction selectPrice(std::string table), retourne la/les valeur(s) du champs "prix_X" de la table "table".
** La fonction selectSchema(std::string table), retourne la/les valeur(s) du champs "schema_X" de la table "table".
** La fonction selectProvider(std::string table), retourne la/les valeur(s) du champs "fournisseur_X" de la table "table".
** La fonction selectColor(std::string table), retourne la/les valeur(s) du champs "couleur_X" de la table "table".
** La fonction selectSizeLaies(std::string table), retourne la/les valeur(s) du champs "tailles_laies_X" de la table "table".
** La fonction selectProviderAdress(), retourne la/les valeur(s) du champs "adresse_f" pour la table Fournisseur uniquement.
** La fonction selectProviderPhone(), retourne la/les valeur(s) du champs "num_f" pour la table Fournisseur uniquement.
**
** Exemple d'appel de la fonction avec affichage du retour de celle-ci :
**
** void  SqlAllSelect(BallonSQL *db)
** {
**   std::vector< std::vector< std::string > > tab;
** 
**   tab = db->selectAll("Accastillage");
**   if (tab.size() == 0)
**      std::cerr << "Error On SelectAll" << std::endl;
**   else
**   {
**    for (unsigned int i=0; i < tab.size(); ++i)
**     {
**       std::cout << "=====================================    Ligne " << i + 1 << "   =====================================" << std::endl;
**       for (unsigned int j=0; j < tab[i].size(); ++j)
**       {
**        std::cout <<  tab[i][j] << std::endl;
**       }
**     } 
**    std::cout << "Fin de l'affichage du tableau" << std::endl;
**   }
** }
*/

#include "BallonSQL.hpp"

std::vector< std::vector< std::string > >	BallonSQL::execSelect(std::string name, std::string table)
{
  std::vector< std::vector< std::string > >	bigTab;
  QString	final_q;
  QSqlQuery	q;

  if (name != "")
    {
      std::string query = "SELECT ";
      
      query += name;
      query += " FROM " + table;
      if (q.exec(final_q.fromStdString(query)))
	{
	  while (q.next())
	    {
	      std::vector< std::string>	tab;
	      for (int x = 0; x < q.record().count(); ++x)
		{
		  tab.push_back(q.value(x).toString().toStdString());
		}
	      bigTab.push_back(tab);
	    }
	}
      else
	{
	  std::cout << "Une erreur s'est produite." << std::endl;
	  std::cerr << q.lastError().text().toStdString() << std::endl;
	}
    }
  return (bigTab);
}

std::vector< std::vector< std::string > >	BallonSQL::selectAll(std::string table)
{
  std::vector< std::vector< std::string > > bigTab;
  QSqlQuery	q;
  QString	final_q;
  std::string query = "SELECT * FROM ";

  query += table;
  if (q.exec(final_q.fromStdString(query)))
    {
      while (q.next())
	{
	  std::vector< std::string>	tab;
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      tab.push_back(q.value(x).toString().toStdString());
	    }
	  bigTab.push_back(tab);
	}
    }
  else
    {
      std::cout << "Une erreur s'est produite." << std::endl;
      std::cerr << q.lastError().text().toStdString() << std::endl;
    }
  return (bigTab);
}

std::vector< std::vector< std::string > >	BallonSQL::selectName(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("nom_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectWeight(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("poids_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectResist(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("resistance_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectResistX(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("resistance_x_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectResistY(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("resistance_y_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectPrice(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("prix_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectSchema(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("schema_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectProvider(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("fournisseur_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectColor(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("couleur_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectSizeLaies(std::string table)
{
  QSqlQuery	q;
  QString	final_q;
  std::string describe = "DESCRIBE ";
  std::string name = "";

  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("tailles_laies_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    std::cerr << q.lastError().text().toStdString() << std::endl;
  return (execSelect(name, table));
}

std::vector< std::vector< std::string > >	BallonSQL::selectProviderAdress()
{
  return (execSelect("adresse_f", "Fournisseurs"));
}

std::vector< std::vector< std::string > >	BallonSQL::selectProviderPhone()
{
  return (execSelect("num_f", "Fournisseurs"));
}
