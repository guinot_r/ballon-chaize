//
// insert.cpp for sql in /home/TaeguKazu/Documents/BallonChaize
// 
// Made by Kevin Dafonseca
// Login   <TaeguKazu@epitech.net>
// 
// Started on  Thu Mar  6 09:17:25 2014 Kevin Dafonseca
// Last update Sun Mar  9 05:00:47 2014 Christian CHAUMERY
//

/*
** Ce fichier contient la/les fonction(s) suivante(s): bool   BallonSQL::execInsert(std::string table_name, std::list< std::string > values)
**
** La fonction execInsert crée la requète de type:
** INSERT INTO table (champ1[, champ2, champ3, ...]) VALUES (value1[, value2, value3, ...])
**
** La fonction execute d'abord la requète suivante :
** SELECT column_name FROM information_schema.columns WHERE table_name ='table'
** 
** Cette requète renvoie le nom des colonnes présent dans la table "table" afin de pouvoir générer les "champ1", "champ2", etc.. dans la requete INSERT INTO ... .
** Il est enregistré autant de champ que de valeurs présentes dans la std::list passé en paramètre si celle-ci contient plus de valeurs que de champ présent dans la table les valeurs suplémentaires seront ignorées.
**
** La fonction retournera TRUE en cas de réussite de l'exécution de la fonction au sein de la base de données, sinon FALSE en cas d'erreur.
**
** Exemple d'appel de la fonction :
**
** void  SqlInsert(BallonSQL *db)
** {
**   std::list <std::string> vlist;
** 
**   vlist.push_back("value1");
**   vlist.push_back("value2");
**   vlist.push_back("value3");
**   vlist.push_back("value4");
**   vlist.push_back("value5");
**   if (db->execInsert("Cordage", vlist) == false)
**      std::cerr << "Error On execInsert" << std::endl;
** }
*/

#include "BallonSQL.hpp"

bool						BallonSQL::execInsert(std::string table_name, std::list< std::string > values)
{
  QString	final_q;
  QSqlQuery	q;
  int		tmp = 0, i = values.size();
  std::string query = "INSERT INTO ";
  std::string qtmp = "SELECT column_name FROM information_schema.columns WHERE table_name ='" + table_name +"'";
  std::string col_string = " (";

  if (q.exec(final_q.fromStdString(qtmp)))
    {
      while (q.next() && tmp < i)
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      col_string += q.value(0).toString().toStdString();
	      col_string += ",";
	    }
	  tmp++;
	}
    }
  else
    {
      std::cout << "Une erreur s'est produite." << std::endl;
      std::cerr << q.lastError().text().toStdString() << std::endl;  
      return (false);
    } 
  col_string.erase(col_string.size() - 1, 1);
  col_string += ")";
  query += table_name + col_string + " VALUES ('";
  while (i > 0)
    {
      query += values.front();
      if ((i-1) != 0)
	query += "','";
      values.pop_front();
      --i;
    }
  query += "')";
  std::cout << query << std::endl;
  if (q.exec(final_q.fromStdString(query)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      std::cout << q.value(x).toString().toStdString() << std::endl;
	    }
	}
    }
  else
    {
      std::cout << "Une erreur s'est produite." << std::endl;
      std::cerr << q.lastError().text().toStdString() << std::endl;  
      return (false);
    }
  return (true);
}

