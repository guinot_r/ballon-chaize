//
// delete.cpp for sql in /home/TaeguKazu/Documents/BallonChaize
// 
// Made by Kevin Dafonseca
// Login   <TaeguKazu@epitech.net>
// 
// Started on  Thu Mar  6 09:18:42 2014 Kevin Dafonseca
// Last update Sun Mar  9 05:00:26 2014 Christian CHAUMERY
//

/*
** Ce fichier contient la/les fonction(s) suivante(s): bool   BallonSQL::DeleteById(std::string table, int id)
**
** La fonction DeleteById crée la requète de type:
** DELETE FROM table    WHERE id_X=id
** avec "X" la lettre associer au nom de la table (ce référencer au MCD pour plus de précision sur la construction des tables).
**
** La fonction execute d'abord un DESCRIBE de la table donnée afin de récuperer la lettre exacte du nom de la colonne id_X.
** Celle-ci sera ensuite exécutée via la méthode exec() de l'objet QSqlQuery nommé "q", cette méthode prend en paramètre la requète précédemment creer.
** La fonction retournera TRUE en cas de réussite de l'exécution de la fonction au sein de la base de données, sinon FALSE en cas d'erreur.
**
** Exemple d'appel de la fonction :
**
** void   SqlDelete(BallonSQL *db)
** {
**        if (db->DeleteById("Cordage", 2) == false)
            std::cerr << "Error on DeleteById" << std::endl;
** }
*/

#include "BallonSQL.hpp"

bool						BallonSQL::DeleteById(std::string table, int id)
{
  QString	final_q;
  QSqlQuery	q;
  std::string	query = "DELETE FROM ";
  std::string	describe = "DESCRIBE ";
  std::string	name = "";
  std::string	s_id;
  std::ostringstream oss;

  oss << id;
  s_id = oss.str();
  describe += table;
  if (q.exec(final_q.fromStdString(describe)))
    {
      while (q.next())
	{
	  for (int x = 0; x < q.record().count(); ++x)
	    {
	      if (q.value(x).toString().toStdString().find("id_") != std::string::npos)
		name = q.value(x).toString().toStdString();
	    }
	}
    }
  else
    {
      std::cerr << q.lastError().text().toStdString() << std::endl;
      return (false);
    }
  query += table;
  query += " WHERE " + name + "=" + s_id;
  std::cout << query << std::endl;
  if (q.exec(final_q.fromStdString(query)))
    {
      while (q.next())
	{
	}
    }
  else
    {
      std::cerr << q.lastError().text().toStdString() << std::endl;
      return (false);
    }
  return (true);
}
