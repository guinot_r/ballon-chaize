//
// main.cpp for BallonChaize in /home/chaume_c/Projects/Other/QT_test
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Thu Dec  5 11:40:14 2013 christian chaumery
// Last update Sun Mar  9 05:07:50 2014 Christian CHAUMERY
//

#include	<QApplication>
#include	"MainWindow.hpp"

int	main(int ac, char **av)
{
  QApplication	app(ac, av);
  MainWindow	mainWin;

  mainWin.show();
  return app.exec();
}
