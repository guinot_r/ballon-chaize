//
// MainWindow.cpp for BallonChaize in /home/chaume_c/Projects/Other/QT_test
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Tue Dec 10 13:36:24 2013 christian chaumery
// Last update Sun Mar  9 05:25:30 2014 Christian CHAUMERY
//

#include	"MainWindow.hpp"

MainWindow::MainWindow() : QMainWindow()
{
  QMenu			*menu;
  QDesktopWidget	dw;

  menu = menuBar()->addMenu("&File");
  menu->addAction("New Profile", this, SLOT(newProfile()), QKeySequence("Ctrl+N"));
  menu->addAction("Open Profile", this, SLOT(openProfile()), QKeySequence("Ctrl+O"));
  menu->addAction("Save Profile", this, SLOT(saveProfile()), QKeySequence("Ctrl+S"));
  menu->addAction("Save Profile As", this, SLOT(saveProfileAs()), QKeySequence("Ctrl+Alt+S"));
  menu->addAction("Quit", qApp, SLOT(quit()), QKeySequence("Ctrl+Q"));
  menu = menuBar()->addMenu("&Edit");
  menu = menuBar()->addMenu("&Help");

  _tabWidget = new QTabWidget();
  _tabWidget->addTab(new ProfileTab, "Profile");
  _tabWidget->addTab(new MaterialTab, "Material");
  _tabWidget->addTab(new DecoTab, "Decoration");
  _tabWidget->addTab(new ColorTab, "Color");
  _tabWidget->addTab(new ProductTab, "Product");
  //  _tabWidget->addTab(new Tab("Mise a plat"), "Mise a plat");
  _tabWidget->setTabPosition(QTabWidget::West);

  resize(dw.width() * 0.5, dw.height() * 0.5);
  move((dw.width() - width()) * 0.5, (dw.height() - height()) * 0.5);
  setCentralWidget(_tabWidget);
  init_db();
};

MainWindow::~MainWindow()
{
  delete _tabWidget;
}

void	MainWindow::init_db()
{
  BallonSQL	db("localhost", "root", "alacon", "Ballon_Chaize");

  if (db.open())
      std::cout << "Vous êtes maintenant connecté à " << db.hostName() << std::endl;
  else
    {
      QMessageBox::critical(NULL, "Database connection error", db.lastError().c_str());
    }
}

void	MainWindow::newProfile()
{
}

void	MainWindow::openProfile()
{
  QString filename = QFileDialog::getOpenFileName(this, tr("Open Profile"), "", tr("Files (*.pro)"));

  _fileName = filename.toStdString();
}

void	MainWindow::saveProfile()
{

}

void	MainWindow::saveProfileAs()
{
  QString filename = QFileDialog::getSaveFileName(this, tr("Open Profile"), "", tr("Files (*.pro)"));

  _saveName = filename.toStdString();
}
