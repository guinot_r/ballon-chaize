#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <librsvg/rsvg.h>
#include <librsvg/rsvg-cairo.h>	

#include "cairo-rendering.h"

cairo_t* create_cairo_context (int               width,
                      int               height,
                      int               channels,
                      cairo_surface_t** surf,
                      unsigned char**   buffer)
{
  cairo_t* cr;
  RsvgHandle *handle;
  GError *error = NULL;
	
  // create cairo-surface/context to act as OpenGL-texture source
  *buffer = calloc (channels * width * height, sizeof (unsigned char));
  if (!*buffer)
    {
      printf ("create_cairo_context() - Couldn't allocate buffer\n");
      return NULL;
    }
	
  *surf = cairo_image_surface_create_for_data (*buffer,
					       CAIRO_FORMAT_ARGB32,
					       width,
					       height,
					       channels * width);
  if (cairo_surface_status (*surf) != CAIRO_STATUS_SUCCESS)
    {
      free (*buffer);
      printf ("create_cairo_context() - Couldn't create surface\n");
      return NULL;
    }

  cr = cairo_create (*surf);

  handle = rsvg_handle_new_from_file ("test.svg", &error);
  if (error != NULL)
    {
      printf("ERROR bad file ?\n");
    }
  rsvg_handle_render_cairo(handle, cr);
  if (cairo_status (cr) != CAIRO_STATUS_SUCCESS)
    {
      free (*buffer);
      printf ("create_cairo_context() - Couldn't create context\n");
      exit(0);
      return NULL;
    }
  return cr;
}

