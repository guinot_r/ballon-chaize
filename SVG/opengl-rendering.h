#ifndef _OPENGL_RENDERING_H
#define _OPENGL_RENDERING_H

void init_gl (void);

void draw_func (int            width,
           int            height,
           unsigned char* surf_data,
           unsigned int   texture_id);

void resize_func (int           width,
             int           height,
             unsigned int* texture_id);

#endif // _OPENGL_RENDERING_H

