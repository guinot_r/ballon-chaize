#ifndef _CAIRO_RENDERING_H
#define _CAIRO_RENDERING_H

#include <cairo.h>

void cairo_image_surface_blur (cairo_surface_t* surface,
			  int              horz_radius,
			  int              vert_radius);

void cairo_image_surface_rblur (cairo_surface_t* surface,
                           int              center_x,
                           int              center_y,
			   double           radius);

cairo_t* create_cairo_context (int               width,
                      int               height,
                      int               channels,
                      cairo_surface_t** surf,
                      unsigned char**   buffer);

#endif // _CAIRO_RENDERING_H

