#ifndef _SIDEVIEW_H_
#define _SIDEVIEW_H_

#include "AView.hpp"

class SideView : public AView
{
public:
  SideView(Montgolfier*);
  virtual ~SideView() {};
protected:
  virtual void	initializeGL();
  virtual void	resizeGL(int w, int h);
  virtual void	paintGL();
  virtual void	mousePressEvent(QMouseEvent *event);
};

#endif /* _SIDEVIEW_H_ */
