//
// DecoTab.hpp for Material in /home/chaume_c/Projects/ballon-chaize
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Wed Jan 15 23:22:20 2014 christian chaumery
// Last update Wed Jan 15 23:25:00 2014 christian chaumery
//

#ifndef		DECOTAB_HPP_
# define	DECOTAB_HPP_

# include	<QWidget>

class		DecoTab : public QWidget
{
public:
  DecoTab();
  ~DecoTab();
};

#endif		/* !DECOTAB_HPP_ */
