//
// ColorTab.hpp for Material in /home/chaume_c/Projects/ballon-chaize
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Wed Jan 15 23:22:20 2014 christian chaumery
// Last update Wed Jan 15 23:26:30 2014 christian chaumery
//

#ifndef		COLORTAB_HPP_
# define	COLORTAB_HPP_

# include	<QWidget>

class		ColorTab : public QWidget
{
public:
  ColorTab();
  ~ColorTab();
};

#endif		/* !COLORTAB_HPP_ */
