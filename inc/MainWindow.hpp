//
// MainWindow.hpp for BallonChaize in /home/chaume_c/Projects/Other/QT_test
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Tue Dec 10 13:36:22 2013 christian chaumery
// Last update Sun Mar  9 05:12:52 2014 Christian CHAUMERY
//

#ifndef		MAINWINDOW_HPP_
# define	MAINWINDOW_HPP_

# include	<list>
# include	<string>
# include	<QString>
# include	<QApplication>
# include	<QMainWindow>
# include	<QDesktopWidget>
# include	<QMenuBar>
# include	<QTabWidget>
# include	<QWidget>
# include	<QMessageBox>
# include	<QFileDialog>
# include	<QAction>
# include	"ProfileTab.hpp"
# include	"MaterialTab.hpp"
# include	"DecoTab.hpp"
# include	"ColorTab.hpp"
# include	"ProductTab.hpp"
# include	"BallonSQL.hpp"

class		MainWindow : public QMainWindow
{
  Q_OBJECT

  QTabWidget		*_tabWidget;

  std::string	_fileName;
  std::string	_saveName;

private:
  void	init_db();

public:
  MainWindow();
  ~MainWindow();

public slots:
  void	newProfile();
  void	openProfile();
  void	saveProfile();
  void	saveProfileAs();
};

#endif		/* !MAINWINDOW_HPP_ */
