#ifndef __SURFACE_HH_
#define __SURFACE_HH_

# include						<glm/glm.hpp>
# include						<vector>

class Surface
{
private:
  std::vector<std::vector<glm::vec4> >			_matrix;
  glm::vec4						**_points;
  unsigned int						_precision;

public:
  Surface();
  Surface(std::vector<std::vector<glm::vec4> > const &);
  virtual ~Surface();
  void			setMatrix(std::vector<std::vector<glm::vec4> > const &);
  std::vector<std::vector<glm::vec4> > const &		getMatrix() const;
  void							setPoints(glm::vec4 **);
  glm::vec4						**getPoints() const;
  void							build(unsigned int);
  glm::vec4						getPatchPoint(double, double);
  void							draw();

private:
  void							desctructPoints();
};

#endif /* __SRUFACE_HH_ */
