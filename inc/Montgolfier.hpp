#ifndef _MONTGOLFIER_H_
#define _MONTGOLFIER_H_

#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <iostream>
#include <glm/glm.hpp>
#include <list>
#include	"Surface.hh"
#include	"CastelJau.hh"
#include	"Flattening.hh"
#include	"SpindleGenerator.hh"

class Montgolfier
{
public:
  Montgolfier();
  virtual ~Montgolfier();
  void init();
  void draw(float x, float y, float z, float lookY);
  void setCtrlPoints(std::list<glm::vec2> const&_ctrlPoints);
private:
  std::list<glm::vec2> _ctrlPoints;
  bool		_isRunning;
  float		_degreeRotationX;
  float		_degreeRotationY;
  int		_nbFuseau;
  float		_zoom;
  Surface	*_surface;
  CastelJau	_casteljau;
  unsigned int	_precision;
  std::vector<glm::vec4>				_controlPoints;
  std::vector<glm::vec2>				_curve;
  std::vector<glm::vec2>				_shapeLiness;
  std::vector< std::vector<glm::vec4> >			_matrix;
  SpindleGenerator					_spindleGenerator;
  std::vector<std::vector<std::vector<glm::vec4> > >	_spindles;
};

#endif /* _MONTGOLFIER_H_ */
