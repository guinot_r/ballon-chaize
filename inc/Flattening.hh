#ifndef __FLATTENING_H_
#define __FLATTENING_H_

# include						<glm/glm.hpp>

class Flattening
{
public:
  static glm::vec4	**generate(glm::vec4 **, unsigned int, unsigned int);
};

#endif /* __FLATTENING_H_ */
