#ifndef _GLWINDOWS_H_
#define _GLWINDOWS_H_

# include	<QtOpenGL>
# include	<GL/glu.h>
# include	<QGLWidget>
# include	<QGridLayout>
# include	"Montgolfier.hpp"

class	AView : public QGLWidget
{
public:
  virtual ~AView() {};
  virtual void	initializeGL() = 0;
  virtual void	resizeGL(int w, int h) = 0;
  virtual void	paintGL() = 0;
  virtual void	mousePressEvent(QMouseEvent *event) = 0;

protected:
  Montgolfier	*_montgol;
  
  
};

#endif /* _GLWINDOWS_H_ */
