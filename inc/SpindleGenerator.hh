#ifndef __SPINDLE_GENERATOR_HH_
#define __SPINDLE_GENERATOR_HH_

# include				<vector>
# include				<glm/glm.hpp>

class SpindleGenerator
{
private:
  std::vector<glm::vec2>		_curve;
  std::vector<glm::vec2>		_shapeLiness;

public:
  SpindleGenerator();
  SpindleGenerator(std::vector<glm::vec2> const &);
  virtual ~SpindleGenerator();
  void					setCurve(std::vector<glm::vec2> const &);
  std::vector<glm::vec2> const		&getCurve() const;
  void					setShapeLiness(std::vector<glm::vec2> const &);
  std::vector<glm::vec2> const		&getShapeLiness() const;
  std::vector<std::vector<std::vector<glm::vec4> > >	getSpindles(unsigned int) const;
};

#endif /* __SPINDLE_GENERATOR_HH_ */
