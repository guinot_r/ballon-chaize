//
// ProfileTab.hpp for Ballon Chaize in /home/chaume_c/Projects/Other/QT_test
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Sat Dec 14 01:39:01 2013 christian chaumery
// Last update Thu Jan 16 02:02:58 2014 christian chaumery
//

#ifndef		PROFILETAB_HPP_
# define	PROFILETAB_HPP_

# include	<QtOpenGL>
# include	<GL/glu.h>
# include	<QGLWidget>
# include	<QGridLayout>
# include	"AView.hpp"
# include	"SideView.hpp"
# include	"GlobalView.hpp"
# include	"UpView.hpp"
# include	"Montgolfier.hpp"
# include	<glm/glm.hpp>
# include	<iostream>

class		ProfileTab : public QWidget
{
public:
  enum Views
    {
      SIDEVIEW = 0,
      UPVIEW = 1,
      GLOBALVIEW = 2
    };

  ProfileTab();
  ~ProfileTab();
  void init();
  void draw();

private:
  QGridLayout	*_mainLayout;
  Montgolfier	*_montgol; 
  std::map<Views, AView *> _views; 

 
};

#endif		/* !PROFILETAB_HPP_ */
