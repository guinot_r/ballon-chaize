//
// MaterialTab.hpp for Material in /home/chaume_c/Projects/ballon-chaize
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Wed Jan 15 23:22:20 2014 christian chaumery
// Last update Wed Jan 15 23:23:41 2014 christian chaumery
//

#ifndef		MATERIALTAB_HPP_
# define	MATERIALTAB_HPP_

# include	<QWidget>

class		MaterialTab : public QWidget
{
public:
  MaterialTab();
  ~MaterialTab();
};

#endif		/* !MATERIALTAB_HPP_ */
