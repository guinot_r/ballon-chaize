#ifndef _UPVIEW_H_
#define _UPVIEW_H_

#include "AView.hpp"

class UpView : public AView
{
public:
  UpView(Montgolfier *);
  virtual ~UpView() {};
protected:
  virtual void	initializeGL();
  virtual void	resizeGL(int w, int h);
  virtual void	paintGL();
  virtual void	mousePressEvent(QMouseEvent *event);
};

#endif /* _UPVIEW_H_ */
