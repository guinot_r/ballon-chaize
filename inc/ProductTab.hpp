//
// ProductTab.hpp for Material in /home/chaume_c/Projects/ballon-chaize
// 
// Made by christian chaumery
// Login   <chaume_c@epitech.net>
// 
// Started on  Wed Jan 15 23:22:20 2014 christian chaumery
// Last update Wed Jan 15 23:29:16 2014 christian chaumery
//

#ifndef		PRODUCTTAB_HPP_
# define	PRODUCTTAB_HPP_

# include	<QWidget>

class		ProductTab : public QWidget
{
public:
  ProductTab();
  ~ProductTab();
};

#endif		/* !PRODUCTTAB_HPP_ */
