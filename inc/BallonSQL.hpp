//
// BallonSQL.hpp for toto in /home/TaeguKazu/Documents/BallonChaize
// 
// Voici mon test 
// 
// Made by Kevin Dafonseca
// Login   <TaeguKazu@epitech.net>
// TROLOLOL
// Started on  Thu Jan 30 02:22:14 2014 Kevin Dafonseca
// Last update Sun Mar  9 05:04:45 2014 Christian CHAUMERY
//

#include <QCoreApplication>
#include <QtSql>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <list>

void	connect();

class	BallonSQL
{
private:
  QSqlDatabase	_db;
  QString	_query;

public:
  BallonSQL(std::string host, std::string user, std::string password, std::string name);
  BallonSQL(BallonSQL const&);
  ~BallonSQL();

  BallonSQL& operator=(BallonSQL const&);
  bool		open();

  std::string	lastError();
  std::string	hostName();
  QSqlDatabase	getDb() const;

  std::vector< std::vector< std::string > >	execSelect(std::string name, std::string table);
  std::vector< std::vector< std::string > >	selectAll(std::string table);
  std::vector< std::vector< std::string > >	selectName(std::string table);
  std::vector< std::vector< std::string > >	selectWeight(std::string table);
  std::vector< std::vector< std::string > >	selectResist(std::string table);
  std::vector< std::vector< std::string > >	selectResistX(std::string table);
  std::vector< std::vector< std::string > >	selectResistY(std::string table);
  std::vector< std::vector< std::string > >	selectPrice(std::string table);
  std::vector< std::vector< std::string > >	selectSchema(std::string table);
  std::vector< std::vector< std::string > >	selectProvider(std::string table);
  std::vector< std::vector< std::string > >	selectProviderAdress();
  std::vector< std::vector< std::string > >	selectProviderPhone();
  std::vector< std::vector< std::string > >	selectColor(std::string table);
  std::vector< std::vector< std::string > >	selectSizeLaies(std::string table);

  bool						execUpdate(std::string table_name, std::string col_name, std::string value, std::string where);
  bool						execInsert(std::string table_name, std::list< std::string > values);
  bool						DeleteById(std::string table, int id);
  
};
