#ifndef _GLOBALVIEW_H_
#define _GLOBALVIEW_H_

#include "AView.hpp"

class GlobalView : public AView
{
public:
  GlobalView(Montgolfier *);
  virtual ~GlobalView() {};
protected:
  virtual void	initializeGL();
  virtual void	resizeGL(int w, int h);
  virtual void	paintGL();
  virtual void	mousePressEvent(QMouseEvent *event);
};

#endif /* _GLOBALVIEW_H_ */
