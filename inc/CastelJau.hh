#ifndef __CASTELJAU_HH_
#define __CASTELJAU_HH_

#include			<vector>
# include			<glm/glm.hpp>

class CastelJau
{
private:
  std::vector<glm::vec4>	_controlPoints;
  double			_precision;

public:
  CastelJau() {}
  CastelJau(std::vector<glm::vec4> const &, double);
  ~CastelJau();
  void			setControlPoints(std::vector<glm::vec4> const &);
  std::vector<glm::vec4> const &getControlPoints() const;
  void				setPrecision(double);
  double			getPrecision() const;
  void				draw();
  glm::vec4			interpolation(glm::vec4, glm::vec4, double);
  glm::vec4			getCasteljauPoint(int, int, double);
};

#endif /* _CASTELJAU_HH_ */
