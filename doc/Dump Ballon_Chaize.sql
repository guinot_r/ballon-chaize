-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 21 Mars 2014 à 09:06
-- Version du serveur: 5.5.35-MariaDB
-- Version de PHP: 5.5.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `Ballon_Chaize`
--

-- --------------------------------------------------------

--
-- Structure de la table `Accastillage`
--

CREATE TABLE IF NOT EXISTS `Accastillage` (
  `id_a` int(11) NOT NULL AUTO_INCREMENT,
  `nom_a` char(255) DEFAULT NULL,
  `poids_a` float DEFAULT NULL,
  `prix_a` float NOT NULL,
  `resistance_a` float NOT NULL,
  `schema_a` varchar(2048) DEFAULT NULL,
  `fournisseur_a` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_a`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `afficher`
--

CREATE TABLE IF NOT EXISTS `afficher` (
  `id_p` int(11) NOT NULL,
  `id_d` int(11) NOT NULL,
  PRIMARY KEY (`id_p`,`id_d`),
  KEY `FK_afficher_id_d` (`id_d`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Coef_Securite`
--

CREATE TABLE IF NOT EXISTS `Coef_Securite` (
  `id_cs` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_cs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Cordage`
--

CREATE TABLE IF NOT EXISTS `Cordage` (
  `id_c` int(11) NOT NULL AUTO_INCREMENT,
  `nom_c` char(255) DEFAULT NULL,
  `poids_c` float DEFAULT NULL,
  `prix_c` float DEFAULT NULL,
  `resistance_c` float DEFAULT NULL,
  `schema_c` varchar(2048) DEFAULT NULL,
  `couleur_c` varchar(1024) DEFAULT NULL,
  `fournisseur_c` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Structure de la table `Decoration`
--

CREATE TABLE IF NOT EXISTS `Decoration` (
  `id_d` int(11) NOT NULL AUTO_INCREMENT,
  `nom_d` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_d`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `fournir`
--

CREATE TABLE IF NOT EXISTS `fournir` (
  `id_f` int(11) NOT NULL,
  `id_a` int(11) NOT NULL,
  `id_c` int(11) NOT NULL,
  `id_t` int(11) NOT NULL,
  PRIMARY KEY (`id_f`,`id_a`,`id_c`,`id_t`),
  KEY `FK_fournir_id_a` (`id_a`),
  KEY `FK_fournir_id_c` (`id_c`),
  KEY `FK_fournir_id_t` (`id_t`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Fournisseurs`
--

CREATE TABLE IF NOT EXISTS `Fournisseurs` (
  `id_f` int(11) NOT NULL AUTO_INCREMENT,
  `nom_f` char(255) DEFAULT NULL,
  `adresse_f` varchar(1024) DEFAULT NULL,
  `num_f` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_f`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Point_de_ctrl_Bezier`
--

CREATE TABLE IF NOT EXISTS `Point_de_ctrl_Bezier` (
  `id_b` int(11) NOT NULL AUTO_INCREMENT,
  `x_b` float DEFAULT NULL,
  `y_b` float DEFAULT NULL,
  `id_p` int(11) NOT NULL,
  PRIMARY KEY (`id_b`),
  KEY `FK_Point_de_ctrl_Bezier_id_p` (`id_p`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Profils`
--

CREATE TABLE IF NOT EXISTS `Profils` (
  `id_p` int(11) NOT NULL AUTO_INCREMENT,
  `galbe_p` int(11) DEFAULT NULL,
  `bouche_p` int(11) DEFAULT NULL,
  `parachute_p` int(11) DEFAULT NULL,
  `volume_p` int(11) DEFAULT NULL,
  `nb_fuseaux_p` int(11) DEFAULT NULL,
  `nb_panneaux_p` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_p`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Projet`
--

CREATE TABLE IF NOT EXISTS `Projet` (
  `id_p` int(11) NOT NULL AUTO_INCREMENT,
  `nom_p` char(255) DEFAULT NULL,
  `id_p_Profils` int(11) NOT NULL,
  PRIMARY KEY (`id_p`),
  KEY `FK_Projet_id_p_Profils` (`id_p_Profils`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Tissu`
--

CREATE TABLE IF NOT EXISTS `Tissu` (
  `id_t` int(11) NOT NULL AUTO_INCREMENT,
  `nom_t` char(255) DEFAULT NULL,
  `poids_t` float DEFAULT NULL,
  `prix_t` float DEFAULT NULL,
  `resistance_x_t` float DEFAULT NULL,
  `resistance_y_t` float DEFAULT NULL,
  `tailles_laies_t` float DEFAULT NULL,
  `schema_t` varchar(2048) DEFAULT NULL,
  `couleur_t` varchar(1024) DEFAULT NULL,
  `fournisseur_t` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_t`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `utiliser`
--

CREATE TABLE IF NOT EXISTS `utiliser` (
  `id_c` int(11) NOT NULL,
  `id_a` int(11) NOT NULL,
  `id_t` int(11) NOT NULL,
  `id_p` int(11) NOT NULL,
  PRIMARY KEY (`id_c`,`id_a`,`id_t`,`id_p`),
  KEY `FK_utiliser_id_a` (`id_a`),
  KEY `FK_utiliser_id_t` (`id_t`),
  KEY `FK_utiliser_id_p` (`id_p`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `afficher`
--
ALTER TABLE `afficher`
  ADD CONSTRAINT `FK_afficher_id_d` FOREIGN KEY (`id_d`) REFERENCES `Decoration` (`id_d`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_afficher_id_p` FOREIGN KEY (`id_p`) REFERENCES `Projet` (`id_p`) ON DELETE CASCADE;

--
-- Contraintes pour la table `fournir`
--
ALTER TABLE `fournir`
  ADD CONSTRAINT `FK_fournir_id_a` FOREIGN KEY (`id_a`) REFERENCES `Accastillage` (`id_a`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_fournir_id_c` FOREIGN KEY (`id_c`) REFERENCES `Cordage` (`id_c`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_fournir_id_f` FOREIGN KEY (`id_f`) REFERENCES `Fournisseurs` (`id_f`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_fournir_id_t` FOREIGN KEY (`id_t`) REFERENCES `Tissu` (`id_t`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Point_de_ctrl_Bezier`
--
ALTER TABLE `Point_de_ctrl_Bezier`
  ADD CONSTRAINT `FK_Point_de_ctrl_Bezier_id_p` FOREIGN KEY (`id_p`) REFERENCES `Profils` (`id_p`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Projet`
--
ALTER TABLE `Projet`
  ADD CONSTRAINT `FK_Projet_id_p_Profils` FOREIGN KEY (`id_p_Profils`) REFERENCES `Profils` (`id_p`);

--
-- Contraintes pour la table `utiliser`
--
ALTER TABLE `utiliser`
  ADD CONSTRAINT `FK_utiliser_id_a` FOREIGN KEY (`id_a`) REFERENCES `Accastillage` (`id_a`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_utiliser_id_c` FOREIGN KEY (`id_c`) REFERENCES `Cordage` (`id_c`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_utiliser_id_p` FOREIGN KEY (`id_p`) REFERENCES `Projet` (`id_p`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_utiliser_id_t` FOREIGN KEY (`id_t`) REFERENCES `Tissu` (`id_t`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
