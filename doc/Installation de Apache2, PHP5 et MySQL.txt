Installing Apache2 With PHP5 And MySQL Support On Fedora 19 (LAMP)

O) Preliminary Note:

You need to install qt4-mysql.
_________________________
|                       |
| yum install qt4-mysql |
|_______________________|

LAMP is short for Linux, Apache, MySQL, PHP. This tutorial shows how you can install an Apache2 webserver on a Fedora 19 server with PHP5 support (mod_php) and MySQL support.

I) To install MySQL, we do this:
__________________________________
|                                |
| yum install mysql mysql-server |
|________________________________|

Then we create the system startup links for MySQL (so that MySQL starts automatically whenever the system boots) and start the MySQL server:
___________________________________
|                                 |
| systemctl enable mysqld.service |
| systemctl start mysqld.service  |
|_________________________________|

Run
_____________________________
|                           |
| mysql_secure_installation |
|___________________________|

to set a password for the user root (otherwise anybody can access your MySQL database!):

[root@server1 ~]# mysql_secure_installation
/usr/bin/mysql_secure_installation: line 379: find_mysql_client: command not found

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none): <-- ENTER
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] <-- ENTER
New password: <-- yourrootsqlpassword
Re-enter new password: <-- yourrootsqlpassword
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] <-- ENTER
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] <-- ENTER
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] <-- ENTER
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] <-- ENTER
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
[root@server1 ~]#

 

II) Installing Apache2
Apache2 is available as a Fedora package, therefore we can install it like this:
_____________________
|                   |
| yum install httpd |
|___________________|

Now configure your system to start Apache at boot time...
__________________________________
|                                |
| systemctl enable httpd.service |
|________________________________|

... and start Apache:
_________________________________
|                               |
| systemctl start httpd.service |
|_______________________________|

Now direct your browser to http://127.0.0.1 , and you should see the Apache2 placeholder page:

Apache's default document root is /var/www/html on Fedora, and the configuration file is /etc/httpd/conf/httpd.conf. Additional configurations are stored in the /etc/httpd/conf.d/ directory.

III) Installing PHP5
We can install PHP5 and the Apache PHP5 module as follows:
___________________
|                 |
| yum install php |
|_________________|

We must restart Apache afterwards:
___________________________________
|                                 |
| systemctl restart httpd.service |
|_________________________________|
 

IV) Testing PHP5 / Getting Details About Your PHP5 Installation
The document root of the default web site is /var/www/html. We will now create a small PHP file (info.php) in that directory and call it in a browser. The file will display lots of useful details about our PHP installation, such as the installed PHP version.
____________________________________
|                                  |
| emacs -nw /var/www/html/info.php |
|__________________________________|

<?php
  phpinfo();
?>

Now we call that file in a browser (e.g. http://127.0.0.1/info.php):

As you see, PHP5 is working, and it's working through the Apache 2.0 Handler, as shown in the Server API line. If you scroll further down, you will see all modules that are already enabled in PHP5. MySQL is not listed there which means we don't have MySQL support in PHP5 yet.

 

V) Getting MySQL Support In PHP5
To get MySQL support in PHP, we can install the php-mysql package. It's a good idea to install some other PHP5 modules as well as you might need them for your applications. You can search for available PHP5 modules like this:
__________________
|                |
| yum search php |
|________________|

Pick the ones you need and install them like this:
_______________________________________________________________________________________________________________________________________________________________________________
|                                                                                                                                                                             |
| yum install php-mysqlnd php-gd php-imap php-ldap php-odbc php-pear php-xml php-xmlrpc php-magickwand php-mbstring php-mcrypt php-mssql php-shout php-snmp php-soap php-tidy |
|_____________________________________________________________________________________________________________________________________________________________________________|

Zend OPcache is a free and open PHP opcode cacher for caching and optimizing PHP intermediate code. It's similar to other PHP opcode cachers, such as APC and Xcache. It is strongly recommended to have one of these installed to speed up your PHP page. Since Zend OPcache is now officially included in PHP 5.5, we use it instead of other opcode cachers.

Zend OPcache can be installed as follows:
___________________________
|                         |
| yum install php-opcache |
|_________________________|

Now restart Apache2:
___________________________________
|                                 |
| systemctl restart httpd.service |
|_________________________________|

Now reload http://127.0.0.1/info.php in your browser and scroll down to the modules section again. You should now find lots of new modules there, including the MySQL module:

VI) phpMyAdmin
phpMyAdmin is a web interface through which you can manage your MySQL databases.

phpMyAdmin can be installed as follows:
__________________________
|                        |
| yum install phpmyadmin |
|________________________|

Now we configure phpMyAdmin. We change the Apache configuration so that phpMyAdmin allows connections not just from localhost (by commenting out everything in the <Directory /usr/share/phpMyAdmin/> stanza and adding the line Require all granted):
_______________________________________________
|                                             |
| emacs -nw /etc/httpd/conf.d/phpMyAdmin.conf |
|_____________________________________________|

# phpMyAdmin - Web based MySQL browser written in php
#
# Allows only localhost by default
#
# But allowing phpMyAdmin to anyone other than localhost should be considered
# dangerous unless properly secured by SSL

Alias /phpMyAdmin /usr/share/phpMyAdmin
Alias /phpmyadmin /usr/share/phpMyAdmin

<Directory /usr/share/phpMyAdmin/>
#   <IfModule mod_authz_core.c>
#     # Apache 2.4
#     <RequireAny>
#       Require ip 127.0.0.1
#       Require ip ::1
#     </RequireAny>
#   </IfModule>
#   <IfModule !mod_authz_core.c>
#     # Apache 2.2
#     Order Deny,Allow
#     Deny from All
#     Allow from 127.0.0.1
#     Allow from ::1
#   </IfModule>
   Require all granted
</Directory>

<Directory /usr/share/phpMyAdmin/setup/>
   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
       Require ip 127.0.0.1
       Require ip ::1
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from 127.0.0.1
     Allow from ::1
   </IfModule>
</Directory>

# These directories do not require access over HTTP - taken from the original
# phpMyAdmin upstream tarball
#
<Directory /usr/share/phpMyAdmin/libraries/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/lib/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/frames/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

# This configuration prevents mod_security at phpMyAdmin directories from
# filtering SQL etc.  This may break your mod_security implementation.
#
#<IfModule mod_security.c>
#    <Directory /usr/share/phpMyAdmin/>
#        SecRuleInheritance Off
#    </Directory>
#</IfModule>

Restart Apache:
___________________________________
|                                 |
| systemctl restart httpd.service |
|_________________________________|

Afterwards, you can access phpMyAdmin under http://127.0.0.1/phpmyadmin/